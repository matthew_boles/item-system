﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface  IISEquipmentSlot { 
        string name { get; set; }
        Sprite icon { get; set; }
	}
}
