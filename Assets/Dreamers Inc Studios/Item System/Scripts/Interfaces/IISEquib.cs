﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface IISEquib {

        // Equip slot
        ISEquipmentSlot Equipment { get; }
        bool equip();
        	
	}
}
