﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface IISStackable
    {
        int Stack(int amount); // default of 0
        int maxStack  { get; }


    }
}
