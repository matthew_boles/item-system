﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface IISGameObject
    {
        GameObject prefab { get; set; }
    }
}
