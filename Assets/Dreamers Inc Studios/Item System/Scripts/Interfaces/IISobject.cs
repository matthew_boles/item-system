﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface IISobject
    {
        string ISName { get; set; }
        int ISValue { get; set; }
        Sprite ISIcon { get; set; }
        int ISBurden { get; set; }

        /*
        equip
        quest item
        durability
        take damage
        prefab

        */
    }
}
