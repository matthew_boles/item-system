﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface  IISDestructible {

        //Durability
        //takeDamage
        int durability { get; }
            int MaxDurability { get; }
        void TakeDamage(int amount);
        void Repair();
        void Break();

     
    }
}
