﻿using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem
{
    public interface IISQuality
    {
        string name { get; set; }
        Sprite Icon { get; set; }

    }
}
