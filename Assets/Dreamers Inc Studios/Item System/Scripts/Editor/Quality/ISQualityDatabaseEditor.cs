﻿using UnityEditor;
using UnityEngine;
using System.Collections;
namespace DreamersIncStudio.ItemSystem.Editor
{
    public partial class ISQualityDatabaseEditor : EditorWindow
    {
        ISQualityDatabase Qualitydb;
      //  ISQuality SelectedItem;
        Texture2D SelectedTexture;
        int SelectedIndex = -1;


        //Button sizes
        const int SPRITE_BUTTON_SIZE = 72;
    
        const string DATABASE_FILE_NAME= @"DISQualityDatabase.asset";
        const string DATABASE_FOLDER_NAME = @"Database";
        const string DATABASE_FULL_PATH = @"Assets/"+DATABASE_FOLDER_NAME+"/"+DATABASE_FILE_NAME;

        [MenuItem(" DIS/Database/Quality Editor %#i")]
        public static void Init() {
            ISQualityDatabaseEditor window = EditorWindow.GetWindow<ISQualityDatabaseEditor>();
            window.minSize = new Vector2(400, 300);
            window.title = "Quality Database";
            window.Show(); 
        }

        void OnEnable() {
            Qualitydb = ScriptableObject.CreateInstance<ISQualityDatabase>();
            Qualitydb = Qualitydb.GetDatabase<ISQualityDatabase>(DATABASE_FOLDER_NAME, DATABASE_FILE_NAME);
            //Qualitydb = AssetDatabase.LoadAssetAtPath(DATABASE_FULL_PATH, typeof(ISQualityDatabase)) as ISQualityDatabase;
            //if (Qualitydb == null)
            //{
            //    if (!AssetDatabase.IsValidFolder("Assets/"+DATABASE_FOLDER_NAME)) {
            //        AssetDatabase.CreateFolder("Assets", DATABASE_FOLDER_NAME);
            //    }
            //    Qualitydb = ScriptableObject.CreateInstance<ISQualityDatabase>();
            //    AssetDatabase.CreateAsset(Qualitydb, DATABASE_FULL_PATH);
            //    AssetDatabase.SaveAssets();
            //    AssetDatabase.Refresh();
           // }
      //      SelectedItem = new ISQuality();
        }
        void OnGUI() {
            ListView();
            //AddQualityToDB();
            GUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(true));
            BottomBar();
            GUILayout.EndHorizontal();

        }

        void BottomBar() {
            //get count
            GUILayout.Label("Qualities:" + Qualitydb.Count);
            if (GUILayout.Button("+Add"))
                Qualitydb.Add(new ISQuality());
        }
     /*   void AddQualityToDB() {
            //name
            SelectedItem.name = EditorGUILayout.TextField("Name:", SelectedItem.name);
            //sprite
            if (SelectedItem.Icon)
                SelectedTexture = SelectedItem.Icon.texture;
            else
                SelectedTexture = null;

            if (GUILayout.Button(SelectedTexture, GUILayout.Width(SPRITE_BUTTON_SIZE), GUILayout.Height(SPRITE_BUTTON_SIZE))) {
                int ControllerID = EditorGUIUtility.GetControlID(FocusType.Passive);
                EditorGUIUtility.ShowObjectPicker<Sprite>(null,true,null,ControllerID);
            }
            string commandname = Event.current.commandName;
            if (commandname == "ObjectSelectorUpdated") {
                SelectedItem.Icon = (Sprite)EditorGUIUtility.GetObjectPickerObject();
                Repaint();
            }
          if(  GUILayout.Button("Save")){
                if (SelectedItem == null|| SelectedItem.name=="")
                    return;
                Qualitydb.Add(SelectedItem);

                SelectedItem = new ISQuality();
            }
       }*/ 
    }
}
