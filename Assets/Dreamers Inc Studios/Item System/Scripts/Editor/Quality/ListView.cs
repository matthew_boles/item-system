﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace DreamersIncStudio.ItemSystem.Editor
{
    public partial class ISQualityDatabaseEditor
    {
        Vector2 _ScrollPos; // Scroll Position for list view


        void ListView()
        {
          _ScrollPos=  EditorGUILayout.BeginScrollView(_ScrollPos, GUILayout.ExpandHeight(true));
            DisplayQualities();
            EditorGUILayout.EndScrollView();
        }
        void DisplayQualities()
        {
            for (int cnt = 0; cnt < Qualitydb.Count; cnt++)
            {
                GUILayout.BeginHorizontal("Box");
                //sprite
                if (Qualitydb.Get(cnt).Icon)
                {
                    SelectedTexture = Qualitydb.Get(cnt).Icon.texture;
                }
                else
                    SelectedTexture = null;

                if (GUILayout.Button(SelectedTexture, GUILayout.Width(SPRITE_BUTTON_SIZE), GUILayout.Height(SPRITE_BUTTON_SIZE)))
                {
                    int ControllerID = EditorGUIUtility.GetControlID(FocusType.Passive);
                    EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, null, ControllerID);
                    SelectedIndex = cnt;
                }
                string commandname = Event.current.commandName;
                if (commandname == "ObjectSelectorUpdated")
                {
                    if (SelectedIndex == -1)
                        return;
                    Qualitydb.Get(SelectedIndex).Icon = (Sprite)EditorGUIUtility.GetObjectPickerObject();
                    SelectedIndex = -1;
                }
                Repaint();
                //name
             //   GUILayout.BeginVertical();
                Qualitydb.Get(cnt).name = GUILayout.TextField(Qualitydb.Get(cnt).name);
                //DelelteKey
                if (GUILayout.Button("x",GUILayout.Width(30)))
                {
                    if (EditorUtility.DisplayDialog("Delete Quality","Are you sure want to delete"
                        +Qualitydb.Get(cnt).name+"from the database?",
                        "Delete","Cancel")) 
                        Qualitydb.Remove(cnt);
                    
                }
                //GUILayout.EndVertical();
                GUILayout.EndHorizontal();
            }
            
        }
    }
}