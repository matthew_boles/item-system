﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DreamersIncStudio.ItemSystem
{
    public class ScriptableObjDatabase<T> : ScriptableObject where T : class
    {
        [SerializeField]
        List<T> database = new List<T>();
        public void Add(T item)
        {
            database.Add(item);
            EditorUtility.SetDirty(this);
        }

        public void insert(int index, T item)
        {
            database.Insert(index, item);
            EditorUtility.SetDirty(this);
        }
        public void Remove(T item)
        {
            database.Remove(item); EditorUtility.SetDirty(this);
        }
        public void Remove(int index)
        {
            database.RemoveAt(index); EditorUtility.SetDirty(this);
        }
        public int Count
        {
            get { return database.Count; }
        }
        public T Get(int index)
        {
            return database.ElementAt(index);
        }
        public void replace(int index, T item)
        {
            database[index] = item; EditorUtility.SetDirty(this);
        }

        public U GetDatabase<U>(string dbpath, string dbname) where U : ScriptableObject
        {
            string dbfullpath = @"Assets/" + dbpath + "/" + dbname;

            U db = AssetDatabase.LoadAssetAtPath(dbfullpath, typeof(U)) as U;
            if (db == null)
            {
                if (!AssetDatabase.IsValidFolder("Assets/" + dbpath))
                {
                    AssetDatabase.CreateFolder("Assets", dbpath);
                }
                db = ScriptableObject.CreateInstance<U>();
                AssetDatabase.CreateAsset(db, dbfullpath);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
            return db;
        }
    }
}
