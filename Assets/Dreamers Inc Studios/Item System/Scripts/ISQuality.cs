﻿using UnityEngine;
using System.Collections;
using System;
namespace DreamersIncStudio.ItemSystem
{
    [System.Serializable]
    public class ISQuality : IISQuality
    {
        [SerializeField]
        string _name;
        [SerializeField]
        Sprite _icon;

       public  ISQuality()
        {
            _icon = new Sprite();
            _name = ""    ;  

        }

        public ISQuality(string name,Sprite icon)
        {
            _icon = icon;
            _name = name;

        }

        public Sprite Icon
        {
            get { return _icon; }
            set
            { _icon = value; }
        }

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
