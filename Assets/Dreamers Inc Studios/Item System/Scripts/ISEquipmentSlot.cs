﻿using UnityEngine;
using System.Collections;
using System;

namespace DreamersIncStudio.ItemSystem
{
    public class ISEquipmentSlot : IISEquipmentSlot
    {
        [SerializeField]
        string _name;
        [SerializeField]
        Sprite _icon;
        public Sprite icon
        {
            get
            { return _icon; }

            set
            { _icon = value; }
        }

        public string name
        {
            get
            { return _name; }

            set
            { _name = value; }
        }
    }
}
