﻿using UnityEngine;
using UnityEditor;
using System.Linq;                  // needed for ElementAt
using System.Collections;
using System.Collections.Generic;

namespace DreamersIncStudio.ItemSystem
{
    public class ISObjectDatabase : ScriptableObjDatabase<ISObject>{}
}
